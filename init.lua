local vim = vim

vim.g.loaded = 1
vim.g.loaded_netrwPlugin = 1

local Variables	= require('variables')
local Settings	= require('settings')
local Events	= require('events')
local Keymaps	= require('keybindings')

local create_autocmd = vim.api.nvim_create_autocmd

local function configure_plugins(function_table)

	local function resulting_func()
		for name, func in pairs(function_table) do
			if packer_plugins[name].loaded then func() end
		end
	end

	create_autocmd({ "VimEnter" }, { callback = resulting_func })
end

Keymaps.general_keybindings()
Settings.init_general()
Events.init_general()
Variables.init_general()

require('packer').startup(function(use)
	use('wbthomason/packer.nvim')
	use('rakr/vim-one')
	use('kyazdani42/nvim-web-devicons')
	use('kyazdani42/nvim-tree.lua')
	use('lewis6991/gitsigns.nvim')
	use {
		'neoclide/coc.nvim',
		branch = 'release'
	}
	use {
		'nvim-treesitter/nvim-treesitter',
		run = ':TSUpdate'
	}
end)

Settings.init_theme()

configure_plugins {
	["coc.nvim"] = Keymaps.coc_keybindings,
	["nvim-web-devicons"] = function ()
		require('nvim-web-devicons').setup {
			color_icons = true
		}
		require('nvim-web-devicons').set_up_highlights()
	end,
	["nvim-tree.lua"] = function ()
		require('nvim-tree').setup {
			renderer = {
				indent_markers = {
					enable = true
				}
			},
		}
		Events.init_nvim_tree()
		Keymaps.nvim_tree_keybindings()
	end,
	["gitsigns.nvim"] = require('gitsigns').setup,
	["nvim-treesitter"] = function ()
		require('nvim-treesitter.configs').setup {
			sync_install = false,
			auto_install = false,
			highlight = {
				enable = true,
				additional_vim_regex_highlighting = false
			}
		}
		Settings.init_treesitter()
	end,
}
