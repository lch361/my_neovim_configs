local vim = vim

local api = vim.api
local o = vim.o
local wo = vim.wo
local M = {}

M.init_general = function ()
	api.nvim_create_autocmd(
		{ "BufNew" },
		{
			pattern = { "*.xml", "*.html" },
			command = "setlocal matchpairs+=<:>"
		}
	)
	api.nvim_create_autocmd(
		{ "FileType" },
		{
			pattern = { "json" },
			command = "set filetype=jsonc"
		}
	)
	-- Event to turn off line highlight on inactive buffers
	api.nvim_create_autocmd(
		{ "WinEnter" },
		{ callback = function ()
			wo.cursorline     = true
			if wo.number == true then wo.relativenumber = true end
		end }
	)
	api.nvim_create_autocmd(
		{ "WinLeave" },
		{ callback = function ()
			wo.cursorline     = false
			if wo.relativenumber == true then
				wo.relativenumber = false
				wo.number = true
			end
		end }
	)
end

-- To hide cursor inside nvim-tree
M.init_nvim_tree = function()
	api.nvim_create_autocmd(
		{ "BufEnter" },
		{
			pattern = { "NvimTree*" },
			callback = function () o.guicursor = "a:block-CursorLine" end
		}
	)
	api.nvim_create_autocmd(
		{ "BufLeave" },
		{
			pattern = { "NvimTree*" },
			callback = function () o.guicursor = "n-v-c-sm:block,i-ci-ve:ver25,r-cr-o:hor20" end
		}
	)
end

return M
