local vim = vim

local v  = vim.v
local o  = vim.o
local wo = vim.o

local function get_spaces(fold_level)
	return string.rep(" ", fold_level * vim.bo.tabstop)
end

local function format_lines(line_amount)
	return table.concat { "", line_amount, ": " }
end

return function ()
	local indent      = get_spaces(v.foldlevel - 1)
	local line_amount = format_lines(v.foldend - v.foldstart + 1)

	-- local remaining_len = tonumber(wo.cc) - indent:len() - line_amount:len() - 3

	local first_line  = vim.call("getline", v.foldstart)
	local last_line   = vim.call("trim", vim.call("getline", v.foldend))
	local spaces      = string.rep(" ", o.columns)

	return table.concat {
		indent, line_amount, first_line, "...", last_line, spaces
	}
end
