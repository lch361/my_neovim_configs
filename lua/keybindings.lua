local vim = vim
local M = {}

local map = vim.keymap.set

M.general_keybindings = function ()
	map('n', '<C-\\>', ':let @/=""<Enter>', { silent = true })
	map('i', ',.', '<Esc>')
	map('i', '.,', '<Esc>')
end

M.coc_keybindings = function ()
	local opts = { silent = true, noremap = true, replace_keycodes = false,
                   expr = true }
	-- Code completion
	map('i', '<Tab>',   'coc#pum#visible() ? coc#pum#next(1)   : \"\\<Tab>\"',   opts)
	map('i', '<Enter>', 'coc#pum#visible() ? coc#pum#confirm() : \"\\<Enter>\"', opts)
	-- Diagnostic navigation
	map('n', '[g', '<Plug>(coc-diagnostics-prev)', { silent = true })
	map('n', ']g', '<Plug>(coc-diagnostics-next)', { silent = true })
	-- Goto defs & decls
	map('n', 'gd', '<Plug>(coc-definition)',       { silent = true })
	map('n', 'gy', '<Plug>(coc-type-definition)',  { silent = true })
	map('n', 'gi', '<Plug>(coc-implementation)',   { silent = true })
	map('n', 'gr', '<Plug>(coc-references)',       { silent = true })
	-- Turn on hovering
	map('n', 'K', ':call CocActionAsync(\'doHover\')<Enter>', { silent = true })
	-- Symbol renaming
	map('n', '<leader>rn', '<Plug>(coc-rename)')
	-- Formatting code
	map('n', '<leader>f', '<Plug>(coc-format-selected)')
	map('x', '<leader>f', '<Plug>(coc-format-selected)')
	-- Scrolling float popups
	map('i', '<A-j>', 'coc#float#has_float() ? coc#float#scroll(1) : \"\\<A-j>\"', opts)
	map('i', '<A-k>', 'coc#float#has_float() ? coc#float#scroll(0) : \"\\<A-k>\"', opts)
end

M.nvim_tree_keybindings = function ()
	map('n', '<C-w><C-t>', ':NvimTreeToggle<Enter>', { silent = true })
end

M.dap_keybindings = function ()
	map('n', '<F11>', require("dap").toggle_breakpoint,  { silent = true })
	map('n', '<F5>',  require("dap").continue,           { silent = true })
	map('n', '<F6>',  require("dap").step_into,          { silent = true })
	map('n', '<F7>',  require("dap").step_over,          { silent = true })
	map('n', '<F8>',  require("dap").step_out,           { silent = true })
	map('n', '<F9>',  require("dap").restart,            { silent = true })
	map('n', '<F10>', require("dap").stop,               { silent = true })
end

M.dap_ui_keybindings = function ()
	map('n', '<F4>',  require("dapui").toggle,           { silent = true })
end

return M
