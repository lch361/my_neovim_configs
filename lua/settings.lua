local vim = vim
local o   = vim.o
local api = vim.api

local M = {}

M.init_general = function ()
	o.title         = true -- Title of the terminal
	o.laststatus    = 3 -- Always show one global statusbar
	o.showtabline   = 2 -- Always show tab bar at the top
	o.termguicolors = true -- Set ability to display true color
	o.background    = 'dark'
	o.timeoutlen    = 200 -- Time for registering sequential key presses

	o.number         = true -- Numbers of each line
	o.relativenumber = true -- Numbers, but relative to cursor
	o.signcolumn     = 'yes' -- Diagnostics signs
	o.cursorline     = true -- Highlight line the cursor is on
	o.list           = true -- Various chars for special symbols
	--o.listchars      = "nbsp:+,tab:▏ ,trail:-"
	o.listchars      = "nbsp:+,tab:  ,trail:-"
	o.fillchars      = "eob: "
	o.colorcolumn    = '80' -- Special line on a background for coding style
	o.wrap           = false

	-- Set the behaviour of tab
	o.tabstop     = 4
	o.shiftwidth  = 4
	o.softtabstop = 4

	o.mouse = ""

    _G.build_statusline = require('statusline')
    vim.o.statusline = "%{%v:lua.build_statusline()%}"

    _G.generate_foldtext = require('foldtext')
    vim.o.foldtext = "v:lua.generate_foldtext()"
end

M.init_theme = function ()
	vim.cmd("colorscheme one")
	api.nvim_set_hl(0, "Normal",    { guibg = nil })
	api.nvim_set_hl(0, "Folded",    { fg = "#abb2bf", bg = "#353b47" })
	api.nvim_set_hl(0, "VertSplit", { fg = "#3e4452", bg = "#3e4452" })
end

M.init_treesitter = function ()
	o.foldmethod = "expr"
	o.foldexpr   = "nvim_treesitter#foldexpr()"
	o.foldlevel  = 20
end

return M
