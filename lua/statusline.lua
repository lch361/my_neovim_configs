local vim = vim

local Style = require('style')

local o    = vim.o
local b    = vim.b
local bo   = vim.bo
local api  = vim.api
local env  = vim.env

local width_stages = {
	extra_short = 40,  -- There will be only shortened Mode, LC and percentage. In the middle, only cwd basename would be visible
	short       = 80,  -- Expand LC. Appear short diagnostics
	middle      = 115, -- Expand cwd to full, but truncate it's length by .. Create short Git. Expand Mode
	long        = 160, -- Display file information, expand diagnostics, increase cwd length
}

local mode_match = setmetatable({
	['n']  = { full = "Normal",    short = 'N'};
	['no'] = { full = "N·Pending", short = 'N·P'};
	['v']  = { full = "Visual",    short = 'V' };
	['V']  = { full = "V·Line",    short = 'V·L'};
	[''] = { full = "V·Block",   short = 'V·B'};
	['s']  = { full = "Select",    short = 'S'};
	['S']  = { full = "S·Line",    short = 'S·L'};
	[''] = { full = "S·Block",   short = 'S·B'};
	['i']  = { full = "Insert",    short = 'I'};
	['ic'] = { full = "Insert",    short = 'I'};
	['R']  = { full = "Replace",   short = 'R'};
	['Rv'] = { full = "V·Replace", short = 'V·R'};
	['c']  = { full = "Command",   short = 'C'};
	['cv'] = { full = "Vim·Ex",    short = 'V·E'};
	['ce'] = { full = "Ex",        short = 'E'};
	['r']  = { full = "Prompt",    short = 'P'};
	['rm'] = { full = "More",      short = 'M'};
	['r?'] = { full = "Confirm",   short = 'C'};
	['!']  = { full = "Shell",     short = 'S'};
	['t']  = { full = "Terminal",  short = 'T'};
}, {
	-- When mode is not recognised, to prevent statusline crashes
	__index = function ()
		return { full = 'Unknown', short = '-'}
	end
})

-- Initialize an unchangeable set of hi groups
api.nvim_create_autocmd({ "ColorScheme", "VimEnter" }, { callback = function ()
	local s = Style.color
	local set_hl = api.nvim_set_hl

	set_hl(0, 'modebar_start', { fg=s[6],  bg=nil,   bold=true })
	set_hl(0, 'modebar',       { fg=s[1],  bg=s[6],  bold=true })
	set_hl(0, 'modebar_s1',    { fg=s[6],  bg=s[14]  })
	set_hl(0, 'modebar_s2',    { fg=s[14], bg=s[1]   })

	set_hl(0, 'error',         { fg=s[2],  bg=s[1]   })
	set_hl(0, 'warning',       { fg=s[4],  bg=s[1]   })
	set_hl(0, 'info',          { fg=s[5],  bg=s[1]   })
	set_hl(0, 'hint',          { fg=s[16], bg=s[1]   })

	set_hl(0, 'git_sl',        { fg=s[1],  bg=s[3]   })
	set_hl(0, 'git_s1',        { fg=s[3],  bg=s[11]  })
	set_hl(0, 'git_s2',        { fg=s[11], bg=s[1]   })

	set_hl(0, 'cwdbar',        { fg=s[7],  bg=s[1]   })

	set_hl(0, 'lcbar_s1',      { fg=s[15], bg=s[1]   })
	set_hl(0, 'lcbar_s2',      { fg=s[7],  bg=s[15]  })
	set_hl(0, 'lcbar',         { fg=s[1],  bg=s[7],  bold=true })
	set_hl(0, 'lcbar_end',     { fg=s[1],  bg=s[7]   })

	set_hl(0, 'file',          { fg=s[5],  bg=s[1]   })

	set_hl(0, 'percent_s1',    { fg=s[12], bg=s[1]   })
	set_hl(0, 'percent_s2',    { fg=s[4],  bg=s[12]  })
	set_hl(0, 'percent',       { fg=s[1],  bg=s[4],  bold=true })
	set_hl(0, 'percent_end',   { fg=s[4],  bg=nil,   bold=true })
end })

local function mode(is_short)
	local chosen_mode = mode_match[vim.call('mode')]
	local body = is_short and chosen_mode.short or chosen_mode.full

	return table.concat {
		"%#modebar_start#",  Style.separator.left_solid,
		"%#modebar#",        body,
		"%#modebar_s1#",     Style.separator.right_solid,
		"%#modebar_s2#",     Style.separator.right_solid,
	}
end

local function diagnostics(is_short)
	local d_info = b.coc_diagnostic_info
	if d_info == nil then
		return ""
	end

	local result = table.concat {
		" %#error# ", d_info['error'],
		" %#warning# ", d_info['warning'],
	}

	if is_short then
		return result
	end

	return table.concat { result,
		" %#info# ", d_info['information'],
		" %#hint# ", d_info['hint']
	}
end

local function git(is_short)
	if b.gitsigns_status_dict == nil then
		return ""
	end

	local body = table.concat { "  ", b.gitsigns_status_dict.head }
	if not is_short then
		body = table.concat { " ", b.gitsigns_status, body }
	end

	return table.concat {
		"%#git_sl#", Style.separator.right_solid, body,
		"%#git_s1#", Style.separator.right_solid,
		"%#git_s2#", Style.separator.right_solid
	}
end

local function cwd(is_short, columns)
	local body = string.gsub(vim.fn.getcwd(), env.HOME, "~")

	if is_short and body ~= "~" then
		local i_last_slash = body:len() - string.find(body:reverse(), '/')
		body = body:sub(i_last_slash + 2)
	end

	local trunc_length = columns
	local flag = bo.modifiable and "" or " "
	local index_to_truncate = body:len() - trunc_length

	if index_to_truncate > 2 then
		body = ".." .. body:sub(index_to_truncate)
	end

	body = body:gsub("/", string.format(" %s ", Style.separator.right_thin))

	return table.concat { "%#cwdbar#", body, flag }
end

local function lines_columns(is_short)
	local body = is_short and "%l|%c " or "%l  %c "

	return table.concat {
		"%#lcbar_s1#",  Style.separator.left_solid,
		"%#lcbar_s2#",  Style.separator.left_solid,
		"%#lcbar#",     body,
		"%#lcbar_end#", Style.separator.left_solid,
	}
end

local function file_attributes()
	local encoding = o.encoding
	local syntax = bo.syntax
	local indent = bo.tabstop

	syntax = syntax == "" and "plain" or syntax
	indent = "tab:" .. indent

	return table.concat {
		"%#file#",
		" ", encoding, " ", Style.separator.left_thin,
		" ", syntax,   " ", Style.separator.left_thin,
		" ", indent,   " "
	}
end

local function percentage()
	return table.concat {
		"%#percent_s1#",  Style.separator.left_solid,
		"%#percent_s2#",  Style.separator.left_solid,
		"%#percent#",     "並%p%%",
		"%#percent_end#", Style.separator.right_solid
	}
end

return function ()
	local cols = o.columns

	local s  = width_stages.short
	local m  = width_stages.middle
	local l  = width_stages.long

	local left_bar = table.concat {
		mode(cols < m),
		cols < s and "" or diagnostics(cols < l),
		cols < m and "" or git(cols < l),
	}
	local center_bar = cwd(cols < m, cols)
	local right_bar = table.concat {
		lines_columns(cols < s),
		cols < l and "" or file_attributes(),
		percentage()
	}

	return table.concat {left_bar, "%=", center_bar, "%=", right_bar, "%*"}
end
