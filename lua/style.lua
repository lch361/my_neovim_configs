return {
	color = {
		"#2c323c", "#e06c75", "#91b974", "#e5c07b",
		"#5799cf", "#bb73d2", "#42b7c7", "#a3aab7",
		"#5c6370", "#ad1f2a", "#52862c", "#b2832a",
		"#135e9c", "#82279f", "#118293", "#ffffff"
	},
	separator = {
		right_solid = '', -- 
		right_thin  = '', -- 
		left_solid  = '', -- 
		left_thin   = '', -- 
	}
}
