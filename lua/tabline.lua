local vim = vim
local o = vim.o
local call = vim.call

local style = require('style')
local devicons = require('nvim-web-devicons')

local createTab = function (tabNumber)
	local icon = devicons.get_icon_by_filetype('cpp')
	return table.concat { "%#TabLine,DevIconC#", icon }
end

_G.createTabline = function ()
	local result = ""
	for iTabNumber = 1, call('tabpagenr', '$') do
		result = result .. createTab(iTabNumber)
	end
	return result
end

o.tabline = "%{%v:lua.createTabline()%}"
