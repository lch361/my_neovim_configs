local g = vim.g
local env = vim.env
local M = {}

M.init_general = function ()
	g.mapleader = ','
	if string.find(env.PATH, env.HOME.."/.local/bin") == fail then
		env.PATH = env.HOME.."/.local/bin:"..env.PATH
	end
end

return M
